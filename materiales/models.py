from django.db import models

from encargados.models import Encargado

class TipoMaterial(models.Model):
    nombre = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Tipo de material"
        verbose_name_plural = "Tipo de material"

    def __str__(self):
        return "Tipo de material: " + self.nombre.title()

class Material(models.Model):
    und_medida = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=1000)
    cod_unspsc = models.CharField(max_length=100)
    nombre = models.CharField(max_length=100)
    aprobado = models.BooleanField(default=False)
    coordinador = models.ForeignKey(Encargado, related_name="coordinador")
    instructor = models.ForeignKey(Encargado, related_name="instructor")
    tipo = models.ForeignKey(TipoMaterial)

    class Meta:
        verbose_name = "Material"
        verbose_name_plural = "Materiales"

    def __str__(self):
        return "Material: " + self.nombre.title()
