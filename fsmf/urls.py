from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^logout/', include('logout_app.urls', namespace="Logout URLs")),
    url(r'^login/', include('login_app.urls', namespace="Login URLs")),
    url(r'^dashboard/', include('dashboard.urls', namespace="Dashboard URLs")),
]
