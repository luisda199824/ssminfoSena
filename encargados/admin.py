from django.contrib import admin

from .models import Encargado

@admin.register(Encargado)
class EncargadoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellido')
