from django.conf.urls import url

from .views import logout_view

urlpatterns = [
    url(r'^$', logout_view, name="Logout Page"),
]
