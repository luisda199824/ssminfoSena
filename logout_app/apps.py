from django.apps import AppConfig


class LogoutAppConfig(AppConfig):
    name = 'logout_app'
