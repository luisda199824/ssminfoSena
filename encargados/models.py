from django.db import models

from django.contrib.auth.models import User

class Encargado(models.Model):
    user = models.ForeignKey(User)
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    correo = models.CharField(max_length=100)
    cedula = models.CharField(max_length=100)
    direccion = models.CharField(max_length=100)
    telefono = models.CharField(max_length=100)
    is_coordinador = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Encargado"
        verbose_name_plural = "Encargados"

    def __str__(self):
        if self.is_coordinador:
            return self.nombre.title() + " " + self.apellido.title()
        else:
            return "Instructor " + self.nombre.title()
