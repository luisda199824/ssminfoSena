from django.db import models

class Area(models.Model):
    nombre = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Área"
        verbose_name_plural = "Áreas"

    def __str__(self):
        return "Área: " + self.nombre
