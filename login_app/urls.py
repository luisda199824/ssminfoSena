from django.conf.urls import url

from .views import login_view, login_lider_view

urlpatterns = [
    url(r'^$', login_view, name="Login URL"),
    url(r'^lider/$', login_lider_view, name="Login lider URL"),
]
