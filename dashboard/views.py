from django.http import HttpResponse
from django.template.loader import get_template
from django.shortcuts import redirect

def dashboard_view(req):
    if req.user is None:
        return redirect('/login/')

    from encargados.models import Encargado

    encargado = Encargado.objects.get(user=req.user)

    template = get_template('dashboard/index.html')
    ctx = {
        'is_coordinador':encargado.is_coordinador
    }
    return HttpResponse(template.render(ctx, req))

def dashboard_lider_view(req):
    if req.user is None:
        return redirect('/login/lider/')

    from lideres.models import Lider
    from materiales.models import Material

    lider = Lider.objects.get(user=req.user)

    template = get_template('dashboard/index_lider.html')
    ctx = {
        'materiales': Material.objects.filter(aprobado=True)
    }
    return HttpResponse(template.render(ctx, req))

def solicitar_view(req):
    if req.user is None:
        return redirect('/login/')

    msg = ""

    from encargados.models import Encargado
    from materiales.models import Material, TipoMaterial

    encargado = Encargado.objects.get(user=req.user)

    if req.method == "POST":
        cod = req.POST.get('cod')
        nombre = req.POST.get('nombre')
        desc = req.POST.get('desc')
        tipo = req.POST.get('tipo')
        unidad = req.POST.get('unidad')
        coordinador = req.POST.get('coordinador')

        args = [
            cod,
            nombre,
            desc,
            tipo,
            unidad,
            coordinador
        ]

        if None in args or "" in args or tipo == -1 or coordinador == -1:
            msg = "Faltan campos por llenar"
        else:
            coordinador = Encargado.objects.get(id=coordinador)
            tipo = TipoMaterial.objects.get(id=tipo)

            material = Material(
                und_medida=unidad,
                descripcion=desc,
                cod_unspsc=cod,
                nombre=nombre,
                aprobado=False,
                coordinador=coordinador,
                instructor=encargado,
                tipo=tipo
            )

            material.save()
            msg = "Creado"

    template = get_template('dashboard/solicitar.html')
    ctx = {
        'tipos': TipoMaterial.objects.all(),
        'coordinadores': Encargado.objects.filter(is_coordinador=True),
        'msg': msg
    }
    return HttpResponse(template.render(ctx, req))

def revisar_view(req):
    if req.user is None:
        return redirect('/login/')

    msg = ""

    from encargados.models import Encargado
    from materiales.models import Material, TipoMaterial

    encargado = Encargado.objects.get(user=req.user)

    template = get_template('dashboard/revisar.html')
    ctx = {
        'materiales': Material.objects.all()
    }
    return HttpResponse(template.render(ctx, req))

def aprobar_view(req, id_material):
    if req.user is None:
        return redirect('/login/')

    msg = ""

    from encargados.models import Encargado
    from materiales.models import Material

    encargado = Encargado.objects.get(user=req.user)

    material = Material.objects.filter(id=id_material)

    if len(material) == 0:
        msg = "Material no existe"
    else:
        if material[0].coordinador == encargado:
            material[0].aprobado = True
            material[0].save()
            msg = "aprobado"
        else:
            msg = "No tiene autorización para aprobar este material"

    template = get_template('dashboard/aprobar.html')
    ctx = {
        'msg': msg
    }
    return HttpResponse(template.render(ctx, req))
