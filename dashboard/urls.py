from django.conf.urls import url

from .views import dashboard_view, solicitar_view, revisar_view
from .views import aprobar_view, dashboard_lider_view

urlpatterns = [
    url(r'^$', dashboard_view, name="Dashboard URL"),
    url(r'^solicitar/$', solicitar_view, name="Dashboard URL"),
    url(r'^revisar/$', revisar_view, name="Dashboard URL"),
    url(r'^aprobar/material/(?P<id_material>\d+)/$', aprobar_view, name="Dashboard URL"),
    url(r'^lider/$', dashboard_lider_view, name="Dashboard lider URL"),
]
