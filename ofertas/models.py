from django.db import models

from areas.models import Area
from encargados.models import Encargado

class Oferta(models.Model):
    nombre = models.CharField(max_length=100)
    ficha = models.CharField(max_length=100)
    area = models.ForeignKey(Area)
    encargado = models.ForeignKey(Encargado)

    class Meta:
        verbose_name = "Oferta"
        verbose_name_plural = "Ofertas"

    def __str__(self):
        return "Oferta: " + self.nombre.title()
