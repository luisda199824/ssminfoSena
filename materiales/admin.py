from django.contrib import admin

from .models import Material, TipoMaterial

@admin.register(Material)
class MaterialAdmin(admin.ModelAdmin):
    list_display = ('cod_unspsc', 'nombre')

@admin.register(TipoMaterial)
class TipoMaterialAdmin(admin.ModelAdmin):
    list_display = ('nombre',)
