from django.http import HttpResponse
from django.template.loader import get_template
from django.shortcuts import redirect

from encargados.models import Encargado

def login_view(req):
    msg = ""

    if req.method == "POST":
        username = req.POST.get('username')
        password = req.POST.get('password')

        from django.contrib.auth import authenticate, login

        user = authenticate(
            username=username,
            password=password
        )

        if user is not None:
            encargado = Encargado.objects.filter(user=user)
            if len(encargado) > 0:
                login(req, user)
                return redirect('/dashboard/')
            else:
                msg = "user not found"
        else:
            encargado = Encargado.objects.filter(user__username=username)
            if len(encargado) == 0:
                msg = "user not found"
            else:
                msg = "bad password"


    template = get_template('login/login.html')
    ctx = {
        'msg': msg,
    }
    return HttpResponse(template.render(ctx, req))

from lideres.models import Lider

def login_lider_view(req):
    msg = ""

    if req.method == "POST":
        username = req.POST.get('username')
        password = req.POST.get('password')

        from django.contrib.auth import authenticate, login

        user = authenticate(
            username=username,
            password=password
        )

        if user is not None:
            lider = Lider.objects.filter(user=user)
            if len(lider) > 0:
                login(req, user)
                return redirect('/dashboard/lider/')
            else:
                msg = "user not found"
        else:
            lider = Lider.objects.filter(user__username=username)
            if len(lider) == 0:
                msg = "user not found"
            else:
                msg = "bad password"


    template = get_template('login/login.html')
    ctx = {
        'msg': msg,
    }
    return HttpResponse(template.render(ctx, req))
