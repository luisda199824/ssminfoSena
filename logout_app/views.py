from django.shortcuts import redirect

def logout_view(req):
    from django.contrib.auth import logout
    logout(req)
    return redirect('/login/')
