from django.apps import AppConfig


class EncargadosConfig(AppConfig):
    name = 'encargados'
