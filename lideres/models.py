from django.db import models

from django.contrib.auth.models import User

class Lider(models.Model):
    user = models.ForeignKey(User)
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    correo = models.CharField(max_length=100)
    cedula = models.CharField(max_length=100)
    direccion = models.CharField(max_length=100)
    telefono = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Lider"
        verbose_name_plural = "Lideres"

    def __str__(self):
        return "Lider " + self.nombre.title()
