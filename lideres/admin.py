from django.contrib import admin

from .models import Lider

@admin.register(Lider)
class LiderAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellido')
